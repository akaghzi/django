from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return render(request, 'home.html')


def help(request):
    return render(request, 'help.html')

def count(request):
    fulltext = request.GET['fulltext']
    wordcountlist = fulltext.split()
    wcdict = {}
    for word in wordcountlist:
        try:
          wcdict[word] = wcdict[word] + 1
        except:
          wcdict[word] = 1
    #print(wcdict)
    return render(request, 'count.html', {'fulltext':fulltext, 'wordcount':len(wordcountlist), 'wcdict':wcdict})

